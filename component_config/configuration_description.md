## Input mapping

- **File Input Mapping**:
    Specify the file using the specific tags
    
## Configuration parameters
This extractor refers to **Skills** as **Workspaces** as this is how they are referred to in the documentation of the API.
 
- **workspace_id**:
    Id of the skill/workspace that you wish to write data to 
  
- **#api_key**:
    You IBM Watson Assistant api key
  
- **version**:
    version of the API you wish to use e.g. 2019-02-28
  

## Sample Configuration
```json
{
    "workspace_id": "ID_OF_WORKSPACE_HERE",
    "#api_key": "API_KEY_HERE",
    "version": "2019-02-28"
}
```