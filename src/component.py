import logging
import json
from ibm_watson_assistant import WatsonAssistantClient, ClientApiException

from keboola.component.base import ComponentBase
from keboola.component.exceptions import UserException

REQUIRED_PARAMETERS = []
REQUIRED_IMAGE_PARS = []

KEY_WORKSPACE_ID = "workspace_id"
KEY_API_KEY = "#api_key"
KEY_WATSON_VERSION = "version"


class Component(ComponentBase):
    def __init__(self):
        super().__init__(required_parameters=REQUIRED_PARAMETERS,
                         required_image_parameters=REQUIRED_IMAGE_PARS)
        params = self.configuration.parameters
        watson_version = params.get(KEY_WATSON_VERSION)
        api_key = params.get(KEY_API_KEY)

        self.watson_client = WatsonAssistantClient(api_key, watson_version)
        self.watson_client.login()

    def run(self):
        params = self.configuration.parameters
        workspace_id = params.get(KEY_WORKSPACE_ID)
        input_file = self.get_input_file()
        with open(input_file.full_path, "r") as f:
            workspace_data = json.load(f)
        self.validate_input(workspace_data, workspace_id)

        # Extractor adds status key to the data, 'status' cannot be pushed back, so drop it
        workspace_data.pop('status', None)

        self.update_workspace_data(workspace_data)

    def update_workspace_data(self, workspace_data):
        try:
            return self.watson_client.update_workspace(workspace_data)
        except ClientApiException as client_exc:
            raise UserException(client_exc) from client_exc

    def get_input_file(self):
        input_files = self.get_input_files_definitions(only_latest_files=True)
        if len(input_files) == 1:
            return input_files[0]
        else:
            raise UserException(
                f"Only 1 input file should be specified, the amount of files received was {len(input_files)}")

    @staticmethod
    def validate_input(workspace_data, workspace_id):
        if "workspace_id" not in workspace_data:
            raise UserException("Key 'workspace_id' is missing in the input JSON")
        if workspace_data["workspace_id"] != workspace_id:
            raise UserException(f"Input file workspace_id ({workspace_data['workspace_id']}) does not match "
                                f"configuration workspace_id ({workspace_id})")


if __name__ == "__main__":
    try:
        comp = Component()
        comp.run()
    except UserException as exc:
        logging.exception(exc)
        exit(1)
    except Exception as exc:
        logging.exception(exc)
        exit(2)
