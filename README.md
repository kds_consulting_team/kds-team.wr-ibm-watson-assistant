IBM Watson Assistant Writer
=============

IBM Watson Assistant is a cloud service that allows developers to embed an artificial intelligence virtual assistant in
the software they are developing and brand the assistant as their own.

This writer enables users to update the settings of specified skills. The setting data of a skill contain intents,
entities, and dialogs.

For more information on skills, entities, intents, and dialogs see the IBM watson documentation.

**Table of contents:**

[TOC]

Functionality notes
===================
This writer refers to **Skills** as **Workspaces** as this is how they are referred to in the documentation of the API.

The writer uses the ibm_watson python sdk utilizing the update_workspace() to update the whole workspace.

Prerequisites
=============

Get the API token of IBM Watson assistant

Configuration
=============

workspace_id
-------
Id of the skill/workspace that you wish to write data to 

#api_key
-------
You IBM Watson Assistant api key

version
-------
version of the API you wish to use

Sample Configuration
=============
```json
{
  "parameters": {
    "workspace_id": "ID_OF_WORKSPACE_HERE",
    "#api_key": "API_KEY_HERE",
    "version": "2019-02-28"
  }
}
```

Use Case 
=============
This writer is set to be used with the extractor component.

You can set up an orchestration of extract > transform > write. 
Add the output json as input of the transformation, and the input of the writer as the output of the transformation.

A sample transformation:

```
import json
from keboola.component.interface import CommonInterface

ci = CommonInterface()

input_file = ci.get_input_files_definitions(only_latest_files=True)[0]

with open(input_file.full_path, "r") as input_file:
	workspace_data = json.load(input_file)
  
for i,intent in enumerate(workspace_data["intents"]):
  workspace_data["intents"][i]["intent"] = f"{intent['intent']}_UPDATED"
  
with open("out/files/updated_WORKSPACE_ID_workspace.json", "w") as output_file:
	workspace_data = json.dump(workspace_data, output_file)
```
Development
-----------

If required, change local data folder (the `CUSTOM_FOLDER` placeholder) path to your custom path in
the `docker-compose.yml` file:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    volumes:
      - ./:/code
      - ./CUSTOM_FOLDER:/data
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Clone this repository, init the workspace and run the component with following command:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
git clone https://bitbucket.org/kds_consulting_team/kds-team.wr-ibm-watson-assistant/src/master/ kds-team.ex-ibm-watson-assistant
cd kds-team.ex-ibm-watson-assistant
docker-compose build
docker-compose run --rm dev
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Run the test suite and lint check using this command:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
docker-compose run --rm test
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Integration
===========

For information about deployment and integration with KBC, please refer to the
[deployment section of developers documentation](https://developers.keboola.com/extend/component/deployment/)
